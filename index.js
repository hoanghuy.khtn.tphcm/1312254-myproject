var app = angular.module('StarterApp', ['ngMaterial','ngAnimate']);

app.controller('AppCtrl', ['$scope', 
'$mdSidenav', function($scope, $mdSidenav){
  $scope.Name = "Phạm Hoàng Huy";
  $scope.Job = "Web Developer";
  $scope.Address = "13/10/11 Lương Thế Vinh, P.Tân Thới Hòa, Q.Tân Phú, TP.HCM";
  $scope.Email = "hoanghuy.khtn.tphcm@gmail.com";
  $scope.Phone = "0128 4884 900";
  $scope.Skill = "Kỹ năng";
  $scope.Favorite = "Sở thích";
  
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
}]);

app.controller('TechniCtrl', ['$scope', function($scope){
  $scope.TechniTitle="Tóm tắt chuyên môn";
  $scope.TechniContent="Tôi có niềm đam mê lớn với IT, đặc biệt là thiết kế và phát triển ứng dụng web, vì thế tôi có thể tiếp cận với một ngôn ngữ lập trình hay ứng dụng một công nghệ mới một cách nhanh chóng. Hoàn thành tốt trách nhiệm của mình là phương châm trong công việc của tôi. Tôi có thể dễ dàng thích nghi với các môi trường làm việc khác nhau. Ngoài ra, sự hiểu biết trong việc áp dụng mô hình MVC* - một công nghệ được ứng dụng rộng rãi trong việc phát triển website."
}]);

app.controller('mySkill', ['$scope', function($scope){
  $scope.skill1="Giao tiếp";
  $scope.myValue1="8";
  $scope.skill2="Làm việc nhóm";
  $scope.myValue2="7";
  $scope.skill3="PHP";
  $scope.myValue3="5";
  $scope.skill4="After effect";
  $scope.myValue4="9";
  $scope.skill5="HTML/CSS";
  $scope.myValue5="8";
  $scope.skill6="Angular JS";
  $scope.myValue6="7";
}]);

app.controller('myFavorite', ['$scope', function($scope){
  $scope.favorite1="Nghe nhạc";
  $scope.favorite2="Chơi game";
  $scope.favorite3="Đọc sách";
  $scope.favorite4="Cafe cùng bạn bè";
  $scope.favorite5="Làm video clip";
  $scope.favorite6="Đi dạo";
}]);

app.factory("tabsFactory",function()
  {
  
  var tabs = [
    {
      title:"Học tập",
      desc1:"Đại học Khoa học tự nhiên TPHCM: (09/2013 - 06/2017)",
      content1:"Học CNTT chuyên ngành Công nghệ phần mềm: thiết kế và phát triển ứng dụng web.",
    },
    {
      title:"Kinh nghiệm làm việc",
      desc1:"Công ty Vinagame Việt Nam: (09/2016 - 07/2017)",
      content1:"Thực tập thiết kế và phát triển website game online."
    },
    { 
      title: "Hoạt động xã hội",
      desc1: "After effect - Kỹ xảo hiệu ứng: (09/2015 - 01/2016)",
      content1:"Nhận làm thuê các sản phẩm quảng cáo, sự kiện."
     },
    
  ];
  
  return tabs;
  
});

app.service("tabsService",function()
{
  this.delete  = function(name)
  {
    var tabs =
        [
          {
            title:name,
            desc:"delete content"
          }
        ];
    
    return tabs;
  }
  
  
});



app.controller("TabCtrl",['$scope','tabsFactory','tabsService',
function($scope,tabsFactory,tabsService)
{
 
  $scope.data = "";
  $scope.data.selectedIndex = 1;
  $scope.tabs = tabsFactory;
  
   $scope.next = function() {
      $scope.data.selectedIndex = Math.min($scope.data.selectedIndex + 1, 2) ;
    };
    $scope.previous = function() {
      $scope.data.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0);
    };
 
  
}] );